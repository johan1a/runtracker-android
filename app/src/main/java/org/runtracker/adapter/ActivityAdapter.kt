package org.runtracker.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.runtracker.R
import org.runtracker.api.ActivityFacade
import org.runtracker.fragment.FragmentNavigator
import java.util.*

class ActivityAdapter(val navigator: FragmentNavigator, var dataSet: List<ActivityFacade> = emptyList()) : RecyclerView.Adapter<ActivityAdapter.ViewHolder>() {

    companion object {
        const val LOG_TAG = "ActivityAdapter"
    }

    val dateFormatter = DateTimeFormat.forPattern("EE HH:mm")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.i(LOG_TAG, "onCreateViewHolder")
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_list_item, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(layout)
    }

    override fun getItemCount(): Int {
        Log.i(LOG_TAG, "getItemCount")
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i(LOG_TAG, "onBindViewHolder, position: $position, text: ${dataSet[position]}")
        val activity = dataSet[position]

        val utcTime = DateTime(activity.dateCreated, DateTimeZone.UTC)
        val localTime = DateTime(utcTime, DateTimeZone.forTimeZone(TimeZone.getDefault()))
        holder.dateText.text = dateFormatter.print(localTime)
        holder.distanceText.text = "${activity.totalDistance} km"
        holder.timeText.text = activity.totalTime


        holder.constraintLayout.clicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { itemPressed(position) },
                        { error -> Log.e(LOG_TAG, "$error") }
                )
    }

    private fun itemPressed(position: Int) {
        Log.i("MainActivity", "recyclerview item $position pressed")
        navigator.goToActivityDetailsScreen(dataSet[position].id)

    }

    class ViewHolder(val constraintLayout: ConstraintLayout) : RecyclerView.ViewHolder(constraintLayout) {
        var dateText: TextView = constraintLayout.findViewById(R.id.activity_item_date)
        var distanceText: TextView = constraintLayout.findViewById(R.id.activity_item_distance)
        var timeText: TextView = constraintLayout.findViewById(R.id.activity_item_time)
    }

}
