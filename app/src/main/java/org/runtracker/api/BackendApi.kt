package org.runtracker.api

import com.google.gson.*
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import org.joda.time.DateTime
import java.lang.reflect.Type


interface BackendApi {

    companion object {

        fun create(): BackendApi {
            val gson = GsonBuilder()
                    .registerTypeAdapter(DateTime::class.java, JsonDeserializer<DateTime> { json, typeOfT, context ->
                        DateTime(json.asString)
                    })
                    .create()
            val retrofit = Retrofit.Builder()
                    .baseUrl("http://192.168.0.11:8081")
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

            return retrofit.create(BackendApi::class.java)
        }
    }

    @POST("/activity")
    fun postActivity(@Body command: ActivityCommand): Completable

    @GET("/activity")
    fun getActivities(): Observable<List<ActivityFacade>>

    @GET("/activity/{id}")
    fun getActivity(@Path("id") id: Long): Observable<ActivityDetailsFacade>

    @POST("/datapoint")
    fun postDatapoint(@Body dataPoint: DataPoint): Completable

}