package org.runtracker.api

class ActivityFacade(val id: Long,
                     val type: ActivityType,
                     val dateCreated: String,
                     val totalDistance: Double,
                     val totalTime: String)
