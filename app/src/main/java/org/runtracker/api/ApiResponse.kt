package org.runtracker.api

data class ApiResponse(val statusCode: String)