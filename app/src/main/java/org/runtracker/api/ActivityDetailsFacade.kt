package org.runtracker.api

import org.joda.time.DateTime

class ActivityDetailsFacade(val id: Long,
                            val type: ActivityType,
                            val dateCreated: DateTime,
                            val totalDistance: Double,
                            val totalTime: String,
                            val status: ActivityStatus,
                            val commands: List<ActivityCommand>,
                            val timeStarted: DateTime,
                            val timeFinished: DateTime,
                            val dataPoints: List<DataPoint>){

}
