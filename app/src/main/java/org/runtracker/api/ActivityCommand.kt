package org.runtracker.api

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.ISODateTimeFormat


class ActivityCommand() {

    companion object {
        val formatter: DateTimeFormatter = ISODateTimeFormat.dateTime()
    }

    lateinit var type: ActivityType
    lateinit var status: ActivityStatus
    lateinit var dateCreated: String

    constructor(type: ActivityType, status: ActivityStatus, dateCreated: DateTime) : this() {
        this.type = type
        this.status = status
        this.dateCreated = formatter.print(dateCreated)
    }

}