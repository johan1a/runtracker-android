package org.runtracker.api

enum class ActivityStatus {
    STARTED,
    PAUSED,
    FINISHED
}