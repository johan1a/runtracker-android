package org.runtracker.api

import java.math.BigDecimal

data class DataPoint(val lat: BigDecimal,
                     val lon: BigDecimal,
                     val accuracy: Float,
                     val dateCreated: String)
