package org.runtracker.service

import android.util.Log
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Period
import org.joda.time.format.PeriodFormatterBuilder
import java.util.concurrent.TimeUnit


class Timer {
    companion object {
        val LOG_TAG: String = "Timer"
        val formatter = PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendHours()
                .appendSeparator(":")
                .appendMinutes()
                .appendSeparator(":")
                .appendSeconds()
                .toFormatter()
    }
    var textView: TextView? = null
    var startTime: DateTime? = null
    var elapsedTime: Period? = null
    var disposable: Disposable? = null

    fun start(startTime: DateTime) {
        this.startTime = startTime
        elapsedTime = Period(startTime, startTime)


        val delay: Long = 100L

        disposable = Observable.interval(delay, TimeUnit.MILLISECONDS)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val now = DateTime.now(DateTimeZone.UTC)
                    elapsedTime = Period(startTime, now)
                    val text = formatter.print(elapsedTime)
                    textView?.text = text
                }
    }

    fun dispose() {
        disposable?.dispose()
    }

}