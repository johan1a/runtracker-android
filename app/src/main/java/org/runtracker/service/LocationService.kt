package org.runtracker.service

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Color.GREEN
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.joda.time.format.ISODateTimeFormat
import org.runtracker.TimeUtil.Companion.getNowUTC
import org.runtracker.api.BackendApi
import org.runtracker.api.DataPoint
import java.math.BigDecimal


@SuppressLint("MissingPermission")
class LocationService(val locationManager: LocationManager) : LocationListener {


    companion object {
        const val CHECK_INTERVAL: Long = 1000 * 10
        const val ACCURACY_THRESHOLD: Float = 60f
        const val LOG_TAG = "LocationService"
    }

    var activityIsActive = false

    var previousLocation: Location? = null
    var totalDistance = 0f

    val backendService by lazy {
        BackendApi.create()
    }

    var debugView: TextView? = null
    var debugGreen: Int = GREEN
    var distanceText: TextView? = null


    init {
        Log.i(LOG_TAG, "Requesting location updates")
    }

    override fun onLocationChanged(location: Location?) {
        Log.i(LOG_TAG, "onLocationChanged")
        location?.let { newLocation: Location ->
            debugView?.setTextColor(Color.BLACK)
            if (isBetterLocation(newLocation, previousLocation)) {
                if (activityIsActive) {
                    Log.i(LOG_TAG, "It's a better location")

                    postDataPoint(newLocation)
                }
            }
        }
    }

    private fun recalculateDistance(newLocation: Location) {
        previousLocation?.let {
            totalDistance += it.distanceTo(newLocation)
        }
        val distanceKm = "%.2f".format(totalDistance / 1000)
        distanceText?.text = "$distanceKm km"
        previousLocation = newLocation
        debugView?.text = "lat: ${newLocation.latitude}, lon: ${newLocation.longitude}"
    }

    private fun postDataPoint(location: Location) {
        val now = getNowUTC()
        val nowString = ISODateTimeFormat.dateTime().print(now)
        val dataPoint = DataPoint(BigDecimal(location.latitude), BigDecimal(location.longitude), location.accuracy, nowString)
        Log.i(LOG_TAG, "Posting DataPoint: $dataPoint")
        backendService.postDatapoint(dataPoint)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.i(LOG_TAG, "DataPoint post response: OK")
                            recalculateDistance(location)
                            debugView?.setTextColor(debugGreen)
                        },
                        { error ->
                            Log.e(LOG_TAG, "DataPoint post response: $error)")
                            debugView?.setTextColor(Color.RED)
                        })
    }

    private fun isBetterLocation(location: Location, currentBestLocation: Location?): Boolean {
        if (location.accuracy > ACCURACY_THRESHOLD) {
            Log.i(LOG_TAG, "Accuracy ${location.accuracy} is too bad, ignoring")
            return false
        }
        if (currentBestLocation == null) {
            return true
        }

        // Check whether the new location fix is newer or older
        val timeDelta = location.time - currentBestLocation.time
        val isSignificantlyNewer = timeDelta > CHECK_INTERVAL
        val isSignificantlyOlder = timeDelta < -CHECK_INTERVAL
        val isNewer = timeDelta > 0

        Log.i(LOG_TAG, "Accuracy: ${location.accuracy}")
        if (isSignificantlyNewer) {
            return true
        } else if (isSignificantlyOlder) {
            return false
        }

        // Check whether the new location fix is more or less accurate
        val accuracyDelta = (location.accuracy - currentBestLocation.accuracy).toInt()
        val isLessAccurate = accuracyDelta > 0
        val isMoreAccurate = accuracyDelta < 0

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true
        } else if (isNewer && !isLessAccurate) {
            return true
        }
        return false
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        Log.i(LOG_TAG, "onStatusChanged, provider: $provider, status: $status, extras: $extras")
    }

    override fun onProviderEnabled(provider: String?) {
        Log.i(LOG_TAG, "onProviderEnabled, provider: $provider")
    }

    override fun onProviderDisabled(provider: String?) {
        Log.i(LOG_TAG, "onProviderDisabled, provider: $provider")
    }

    fun startListening() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this)
    }

}