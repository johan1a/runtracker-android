package org.runtracker.fragment

interface FragmentNavigator{
    fun goToActivityDetailsScreen(id: Long)

}