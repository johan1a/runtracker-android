package org.runtracker.fragment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import net.danlew.android.joda.JodaTimeAndroid
import org.runtracker.R

class MainActivity : AppCompatActivity() {

    var fragment : Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        JodaTimeAndroid.init(this)

        if(fragment == null) {
            val fragmentManager = supportFragmentManager
            fragment = StartFragment()
            Log.i("fragment manager", "replacing fragment")
            fragmentManager.beginTransaction()
                    .add(R.id.main_content, fragment)
                    .addToBackStack("main fragment TODO")
                    .commit()
        }
    }
}
