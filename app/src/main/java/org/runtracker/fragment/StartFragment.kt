package org.runtracker.fragment

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.runtracker.R
import android.location.LocationManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import io.reactivex.schedulers.Schedulers
import org.runtracker.adapter.ActivityAdapter
import org.runtracker.api.BackendApi
import org.runtracker.service.LocationService


class StartFragment : Fragment(), FragmentNavigator {
    companion object {
        private val LOG_TAG: String = "StartFragment"
        private val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 666
    }

    var locationService: LocationService? = null

    var buttonDisposable: Disposable? = null

    private lateinit var viewManager: LinearLayoutManager
    private lateinit var viewAdapter: ActivityAdapter

    val backendApi: BackendApi = BackendApi.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            locationService = LocationService(it.getSystemService(Context.LOCATION_SERVICE) as LocationManager)
            val hasPermission = ContextCompat.checkSelfPermission(it, ACCESS_FINE_LOCATION)
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                locationService?.startListening()
            } else {
                activity.let {
                    requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
                }
            }

        }
    }

    private fun getActivitiesFromBackend() {
        backendApi.getActivities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.i(LOG_TAG, "Got ${it.size} activities from backend")
                            viewAdapter.dataSet = it
                            viewAdapter.notifyDataSetChanged()
                        },
                        { error ->
                            Log.e(LOG_TAG, "Error when fetching activities from backend: $error)")
                        })
    }

    private var recyclerView: RecyclerView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivitiesFromBackend()
        setupRecyclerView(view)
    }

    private fun setupRecyclerView(view: View) {
        viewAdapter = ActivityAdapter(this, emptyList())
        viewManager = LinearLayoutManager(context)
        recyclerView = view.findViewById<RecyclerView>(R.id.previous_activities).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter


        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.i(LOG_TAG, "User granted location permission")
                    locationService?.startListening()
                } else {
                    Log.e(LOG_TAG, "User declined location permission")
                    // TODO do something useful here
                }
                return
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onResume() {
        super.onResume()
        Log.i(LOG_TAG, "onResume")
        val startButton: Button by lazy { view?.findViewById(R.id.button_start_tracking) as Button }
        buttonDisposable = startButton.clicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onStartButtonClicked,
                        { it -> Log.e(LOG_TAG, "$it") }
                )
    }

    fun onStartButtonClicked(t: Unit) {
        Log.i("MainActivity", "Startbutton pressed")

        activity?.let {
            val fragmentManager = it.supportFragmentManager
            val newFragment = TrackingFragment()
            newFragment.previousFragment = this
            locationService?.let {
                newFragment.locationService = it
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.main_content, newFragment)
                    .addToBackStack("TODO")
                    .commit()
        }
    }

    override fun goToActivityDetailsScreen(activityId: Long) {
        backendApi.getActivity(activityId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { activityDetails ->
                            Log.i(LOG_TAG, "Got activity details from backend")
                            activity?.let {
                                val fragmentManager = it.supportFragmentManager
                                val newFragment = ActivityDetailsFragment()
                                newFragment.previousFragment = this
                                newFragment.activityDetails = activityDetails

                                fragmentManager.beginTransaction()
                                        .replace(R.id.main_content, newFragment)
                                        .addToBackStack("TODO")
                                        .commit()
                            }

                        },
                        { error ->
                            Log.e(LOG_TAG, "Error when fetching activities from backend: $error)")
                        })
    }


    override fun onPause() {
        buttonDisposable?.dispose()
        super.onPause()
    }

}
