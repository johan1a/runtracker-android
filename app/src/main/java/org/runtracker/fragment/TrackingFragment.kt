package org.runtracker.fragment

import org.runtracker.service.Timer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import org.runtracker.R
import org.runtracker.TimeUtil
import org.runtracker.TimeUtil.Companion.getNowUTC
import org.runtracker.api.*
import org.runtracker.service.LocationService
import java.util.concurrent.TimeUnit


class TrackingFragment : Fragment() {

    companion object {
        private val LOG_TAG: String = "TrackingFragment"
    }

    val startTime: DateTime = TimeUtil.getNowUTC()

    var previousFragment: StartFragment? = null

    val backendService by lazy {
        BackendApi.create()
    }

    var apiDisposable: Disposable? = null
    var buttonDisposable: Disposable? = null

    val timer = Timer()
    lateinit var locationService: LocationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // todo save start time, and create new locationservice if it is null
        locationService.activityIsActive = true
        locationService.totalDistance = 0f
        locationService.previousLocation = null
        locationService.debugGreen = resources.getColor(R.color.debugGreen)
        locationService.startListening()
        sendStartAPICall()
    }

    override fun onResume() {
        super.onResume()
        timer.start(startTime)
        setupStopButton()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tracking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timer.textView = view.findViewById(R.id.timer_text)
        locationService.debugView = view.findViewById(R.id.location_debug_view)
        locationService.distanceText = view.findViewById(R.id.distance_text)
    }

    private fun setupStopButton() {
        val stopButton: Button by lazy { view?.findViewById(R.id.button_stop_tracking) as Button }
        stopButton.let { button ->
            buttonDisposable = button.clicks()
                    .observeOn(AndroidSchedulers.mainThread())
                    .debounce(100, TimeUnit.MILLISECONDS)
                    .subscribe(
                            this::onStopButtonClicked,
                            { it -> Log.e(LOG_TAG, "$it") }
                    )
        }
    }

    private fun onStopButtonClicked(t: Unit) {
        Log.i(LOG_TAG, "Sending stop message to backend")
        timer.dispose()
        val command = ActivityCommand(ActivityType.RUNNING, ActivityStatus.FINISHED, getNowUTC())
        locationService.activityIsActive = false
        apiDisposable = sendStopAPICall(command)
    }

    private fun sendStartAPICall() {
        Log.i(LOG_TAG, "Sending start message to backend")
        val command = ActivityCommand(ActivityType.RUNNING, ActivityStatus.STARTED, startTime)
        apiDisposable = backendService.postActivity(command)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { Log.i(LOG_TAG, "Got OK response from server") },
                        { error -> Log.e(LOG_TAG, error.toString()) })
    }

    private fun sendStopAPICall(command: ActivityCommand): Disposable? {
        return backendService.postActivity(command)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.i(LOG_TAG, "Got OK response from server")
                            goToPreviousFragment()
                        },
                        { error ->
                            Log.e(LOG_TAG, error.toString())
                            goToPreviousFragment()
                        })
    }

    private fun goToPreviousFragment() {
        dispose()
        activity?.let {
            val fragmentManager = it.supportFragmentManager
            val newFragment = previousFragment
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content, newFragment)
                    .addToBackStack("TODO")
                    .commit()
        }
    }

    override fun onPause() {
        super.onPause()
        dispose()
    }

    private fun dispose() {
        timer.dispose()
        apiDisposable?.dispose()
        buttonDisposable?.dispose()
    }

}
