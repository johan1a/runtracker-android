package org.runtracker.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.runtracker.R
import org.runtracker.api.ActivityDetailsFacade

class ActivityDetailsFragment : Fragment() {
    var previousFragment: StartFragment? = null
    lateinit var  activityDetails: ActivityDetailsFacade

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_activity_details, container, false)
    }

}
