package org.runtracker

import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class TimeUtil {
    companion object {

        fun getNowUTC() = DateTime.now(DateTimeZone.UTC)

        fun getNowUTCAsString() = DateTime.now(DateTimeZone.UTC)
    }

}
