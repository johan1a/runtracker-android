package org.runtracker.api

import org.joda.time.DateTime
import org.junit.Assert.*
import org.junit.Test

class ActivityCommandTest {

    @Test
    fun dateParse() {
        val time = DateTime(2018, 6, 6, 15, 12, 29, 232)
        val command: ActivityCommand = ActivityCommand(type = ActivityType.RUNNING, status = ActivityStatus.STARTED, dateCreated = time)

        assertEquals("2018-06-06T15:12:29.232Z", command.dateCreated)
    }
}